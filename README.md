# Missão
Utilizar nosso conhecimento em negócios, gerenciamento, processos e tecnologia para atingir os resultados pretendidos por nossos clientes e parceiros.

# Visão
Queremos nos tornar uma das maiores e mais conceituadas empresas de consultoria de negócios e tecnologia do Brasil.

# Valores
- Dedicação ao contínuo crescimento e melhoria de produtos, serviços, profissionais, tecnologias, processos, metodologias, qualidade e todas as atividades e itens ligados a nossa empresa.

- Excelência em todas as atividades prestadas por nossa empresa, buscando superar as expectativas do mercado.

- Responsabilidade com os compromissos firmados com colaboradores, funcionários, parceiros, clientes e sociedade.

- Inovação constante em nossos serviços e produtos para ofertar uma qualidade maior a nossos clientes e parceiros.

- Visão para antecipar as possíveis demandas e necessidades do mercado, beneficiando os clientes atendidos com nossa proatividade.

- Justiça e Respeito a nossos funcionários e colaboradores, sempre lhes propiciando as condições adequadas para a realização de seus trabalhos, zelando por seus direitos.